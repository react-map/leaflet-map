// {/* Quán trà sữa trân châu TocoToco - 262 Đường Cầu Giấy, Quan Hoa, Cầu Giấy, Hà Nội 100000, Việt Nam*/ }
// <Marker position={[21.0381817, 105.7786645]}></Marker>
// {/* Feeling Tea số 4 Trần Thái Tông */ }
// <Marker position={[21.0366996, 105.7838573]}></Marker>
// {/* Trà sữa Hàn Quốc Meet And More Hà Nội - 275 Trần Quốc Hoàn, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam */ }
// <Marker position={[21.0389527, 105.7835354]}></Marker>
// {/* Trà sữa DingTea - 180 Đường Cầu Giấy, Quan Hoa, Cầu Giấy, Hà Nội, Việt Nam*/ }
// <Marker position={[21.0357683, 105.7836856]}></Marker>

// {/* Newssky Apartment - 69 Trần Quốc Hoàn, Dịch Vọng Hậu, Cầu Giấy, Hà Nội 100000, Việt Nam */ }
// <Marker position={[21.0378209, 105.7737239]}></Marker>
// {/* OYO 155 Esalen - 49 Trần Quốc Vượng, Dịch Vọng Hậu, Cầu Giấy, Hà Nội 100000, Việt Nam */ }
// <Marker position={[21.0355494, 105.7839007]}></Marker>
// {/* XuMi Hotel - 16B2 Ngõ 217 Mai Dịch, Mai Dịch, Cầu Giấy, Hà Nội, Việt Nam*/ }
// <Marker position={[21.0395548, 105.7767552]}></Marker>
// {/* Hương Thảo Hotel 2 - Ngõ 130 Đường Hồ Tùng Mậu, Mai Dịch, Cầu Giấy, Hà Nội, Việt Nam*/ }
// <Marker position={[21.0395548, 105.7767552]}></Marker>

// {/* Nhà Thuốc Tư Nhân - 3, Phố Trần Quốc Hoàn, Phường Dịch Vọng Hậu, Quận Cầu Giấy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam*/ }
// <Marker position={[21.0395548, 105.7789868]}></Marker>
// {/* Nhà Thuốc Hà Trung - 69 Doãn Kế Thiện, Mai Dịch, Cầu Giấy, Hà Nội, Việt Nam*/ }
// <Marker position={[21.0392344, 105.7794911]}></Marker>
// {/* Nhà Thuốc Bảo Nam - 46, Ngõ 66 Hồ Tùng Mậu, Mai Dịch, Cầu Giấy, Hà Nội, Việt Nam */ }
// <Marker position={[21.0384032, 105.7791156]}></Marker>
// {/* Nhà Thuốc Bình Minh - 18 Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam*/ }
// <Marker position={[21.0375421, 105.7817656]}></Marker>

// {/* BV Đa khoa 5 sai - 19 Doãn Kế Thiện, Mai Dịch, Cầu Giấy, Hà Nội, Việt Nam*/ }
// <Marker position={[21.0400762, 105.779976]}></Marker>
// {/* Bệnh Viện 19 - 8 Bộ Công An - 9 Trần Bình, Mai Dịch, Cầu Giấy, Hà Nội, Việt Nam*/ }
// <Marker position={[21.0332803, 105.7756921]}></Marker>
// {/* Phòng Khám Đa Khoa Hòa Bình - 3 Trần Thái Tông, Dịch Vọng, Cầu Giấy, Hà Nội, Việt Nam */ }
// <Marker position={[21.0353532, 105.7834383]}></Marker>


exports.place = [
   {
      NAME: 'Quán trà sữa trân châu TocoToco',
      ADDRESS: '60 Đường Hồ Tùng Mậu, Mai Dịch, Cầu Giấy, Hà Nội 100000, Việt Nam',
      COORDINATE: [21.0369631, 105.7778121],
      IMAGE_URL: 'https://lh5.googleusercontent.com/p/AF1QipPaKpCDcCIAe5CZfLRdP86TttCuhaea3PDw4bUH=w408-h544-k-no',
      TYPE: 'MILKTEA'
   },
   {
      NAME: 'Feeling Tea',
      ADDRESS: '4 Trần Thái Tông',
      COORDINATE: [21.0358644, 105.7894213],
      IMAGE_URL: 'https://lh5.googleusercontent.com/p/AF1QipPVSI2dg2aESXQLfXYhQZ8SvzOFgXBJSPqgPb0H=w408-h476-k-no',
      TYPE: 'MILKTEA'
   },
   {
      NAME: 'Trà sữa Hàn Quốc Meet And More Hà Nội',
      ADDRESS: '275 Trần Quốc Hoàn, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam',
      COORDINATE: [21.0416403, 105.7830959],
      IMAGE_URL: 'https://lh5.googleusercontent.com/p/AF1QipN1EZmKYvtzDYKo8RetsbXCWJ_zR3zk9Sshl1qB=w408-h476-k-no',
      TYPE: 'MILKTEA'
   },
   {
      NAME: 'Trà sữa Phúc Long',
      ADDRESS: '241 Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam',
      COORDINATE: [21.0364241, 105.7824792],
      IMAGE_URL: 'https://lh5.googleusercontent.com/p/AF1QipM1ZZOWOo-VVFqGVMpBe_xSXq9R3LTFE-rn01Jx=w408-h279-k-no',
      TYPE: 'MILKTEA'
   },
   {
      NAME: 'Newssky Apartment',
      ADDRESS: '69 Trần Quốc Hoàn, Dịch Vọng Hậu, Cầu Giấy, Hà Nội 100000, Việt Nam',
      COORDINATE: [21.0378209, 105.7737239],
      TYPE: 'HOTEL'
   },
   {
      NAME: 'OYO 155 Esalen ',
      ADDRESS: '49 Trần Quốc Vượng, Dịch Vọng Hậu, Cầu Giấy, Hà Nội 100000, Việt Nam',
      COORDINATE: [21.0355494, 105.7839007],
      TYPE: 'HOTEL'
   },
   {
      NAME: 'XuMi Hotel',
      ADDRESS: '16B2 Ngõ 217 Mai Dịch, Mai Dịch, Cầu Giấy, Hà Nội, Việt Nam',
      COORDINATE: [21.0395548, 105.7767552],
      TYPE: 'HOTEL'
   },
   // {
   //    NAME: 'Spring Hotel Hanoi ',
   //    ADDRESS: '100 Phố Dịch Vọng Hậu, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam',
   //    COORDINATE: [21.0333109, 105.7818537],
   //    TYPE: 'HOTEL'
   // },
   {
      NAME: 'Hương Thảo Hotel 2 ',
      ADDRESS: 'Ngõ 130 Đường Hồ Tùng Mậu, Mai Dịch, Cầu Giấy, Hà Nội, Việt Nam',
      COORDINATE: [21.0395548, 105.7767552],
      TYPE: 'HOTEL'
   },
   {
      NAME: 'Nhà thuốc Trâm Anh 3',
      ADDRESS: '5 Tô Hiệu, Nghĩa Tân, Cầu Giấy, Hà Nội, Việt Nam',
      COORDINATE: [21.0371553, 105.7765751],
      TYPE: 'PHARMACY'
   },
   {
      NAME: 'Nhà Thuốc A23',
      ADDRESS: 'Đường Cốm Vòng, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam',
      COORDINATE: [21.0353929, 105.783227],
      TYPE: 'PHARMACY'
   },
   {
      NAME: 'Nhà Thuốc VINFA',
      ADDRESS: '36 Phạm Hùng, Mỹ Đình, Từ Liêm, Hà Nội, Việt Nam',
      COORDINATE: [21.0280802, 105.7793225],
      TYPE: 'PHARMACY'
   },
   {
      NAME: ' Nhà Thuốc Minh Hạnh',
      ADDRESS: '158 Nguyễn Đổng Chi, Mỹ Đình 1, Nam Từ Liêm, Hà Nội, Việt Nam',
      COORDINATE: [21.0358951, 105.7644054],
      TYPE: 'PHARMACY'
   },
   {
      NAME: 'BV Đa khoa 5 sai',
      ADDRESS: '19 Doãn Kế Thiện, Mai Dịch, Cầu Giấy, Hà Nội, Việt Nam',
      COORDINATE: [21.0400762, 105.779976],
      TYPE: 'HOSPITAL'
   },
   {
      NAME: 'Bệnh Viện 19 - 8 Bộ Công An',
      ADDRESS: '9 Trần Bình, Mai Dịch, Cầu Giấy, Hà Nội, Việt Nam',
      COORDINATE: [21.0332803, 105.7756921],
      TYPE: 'HOSPITAL'
   },
   {
      NAME: 'Phòng Khám Đa Khoa Hòa Bình',
      ADDRESS: '3 Trần Thái Tông, Dịch Vọng, Cầu Giấy, Hà Nội, Việt Nam',
      COORDINATE: [21.0353532, 105.7834383],
      TYPE: 'HOSPITAL'
   },
   {
      NAME: 'Bệnh viện Giao Thông Vận Tải',
      ADDRESS: 'Láng Thượng, Đống Đa, Hà Nội, Việt Nam',
      COORDINATE: [21.031669, 105.777176],
      TYPE: 'HOSPITAL'
   },
   {
      NAME: 'Lofita Tea & Coffee Hồ Tùng Mậu',
      ADDRESS: '01 Đường Hồ Tùng Mậu, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam',
      COORDINATE: [21.0377665, 105.7805577],
      IMAGE_URL: 'https://lh5.googleusercontent.com/p/AF1QipP1MmRtZjNkWdhLOJR-tTK3p2KrGeA-8_C76_34=w408-h289-k-no',
      TYPE: 'COFFEE'
   },
   {
      NAME: 'The Coffee House',
      ADDRESS: 'Số 2 Ngõ 181 Đường Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội 100000, Việt Nam',
      COORDINATE: [21.0359437, 105.7834672],
      IMAGE_URL: 'https://lh5.googleusercontent.com/p/AF1QipN8AgoYbqU342YKpXgYVAoaBrQA15CsjvB_et21=w408-h544-k-no',
      TYPE: 'COFFEE'
   },
   {
      NAME: 'New Club Cafe',
      ADDRESS: 'Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam',
      COORDINATE: [21.0387245, 105.7828147],
      IMAGE_URL: 'https://lh5.googleusercontent.com/p/AF1QipMNo6_4YW69d4iqLyupxDLT26n781RW9F9bDsQM=w426-h240-k-no',
      TYPE: 'COFFEE'
   },
   {
      NAME: 'Cafe Sách',
      ADDRESS: '136 Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam',
      COORDINATE: [21.0367032, 105.7841777],
      IMAGE_URL: 'https://lh5.googleusercontent.com/p/AF1QipNKgfCkktE1Q7G5_FEejUK5A_HiDI-IRr1XSb80=w424-h240-k-no',
      TYPE: 'COFFEE'
   },
]
